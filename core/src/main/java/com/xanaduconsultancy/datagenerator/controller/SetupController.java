//
// Copyright (c) 2011-2015 Xanadu Consultancy Ltd., 
//

package com.xanaduconsultancy.datagenerator.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.xanaduconsultancy.datagenerator.model.Config;

@Controller
public class SetupController {

    private static final Logger LOG = LoggerFactory.getLogger(SetupController.class);

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/setup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void setup(@Valid @RequestBody List<Config> config) {
        LOG.info("Config received: " + config);

    }

    @ResponseBody
    @ExceptionHandler({MethodArgumentNotValidException.class, HttpMessageNotReadableException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Map<String, String> validationError(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        for (FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
            errors.put(fieldError.getField(), fieldError.getDefaultMessage());
        }
        for (ObjectError objectError : ex.getBindingResult().getGlobalErrors()) {
            errors.put(objectError.getObjectName(), objectError.getDefaultMessage());
        }
        return errors;
    }

    @ResponseBody
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Map<String, String> generalError(Exception ex) {
        LOG.error("Error processing request", ex);
        Map<String, String> errors = new HashMap<>();
        errors.put("setupRequest", ex.getMessage());
        return errors;
    }
}
