package com.xanaduconsultancy.datagenerator.strategies;

import java.util.Map;
import java.util.Random;

public interface Strategy {

    <T> T generateData(Map<String,Object> params, Class<T> type);
    void setRandom(Random random);
}
