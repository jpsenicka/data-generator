package com.xanaduconsultancy.datagenerator.strategies;

import java.util.Map;
import java.util.Random;

public class DictionaryStrategy implements Strategy{
    
    private Random random;
    
    public <T> T generateData(Map<String, Object> params, Class<T> type) {
        String value=(String) params.get("value");
        String uri=(String) params.get("uri");
        if(uri != null){
            //call the url
            throw new UnsupportedOperationException("Getting dictionary from a uri is not supported yet");
        }else{
            Object[] embeddedDisctionary=(Object[]) params.get("embeddedDictionary");
            if(embeddedDisctionary instanceof String[]){
                String res=new String(value);
                while(res.contains("$string")){
                    res=res.replaceFirst("$string", generateString((String[]) embeddedDisctionary));
                } 
                return (T)res;
            }else if(embeddedDisctionary instanceof Integer[]){
                String res=new String(value);
                while(res.contains("$int")){
                    res=res.replaceFirst("$int", generateInteger((Integer[]) embeddedDisctionary));
                }
                try{
                    return (T) Integer.valueOf(res);
                }catch(NumberFormatException e){
                    return (T) res;
                }
            }
        }
        
        return null;
    }
    
    private String generateString(String[] embeddedDisctionary){
        Integer r=((random.nextInt())%(embeddedDisctionary.length));
        return embeddedDisctionary[r];
    }
    
    private String generateInteger(Integer[] embeddedDisctionary) {
        Integer r=((random.nextInt())%(embeddedDisctionary.length));
        return String.valueOf(embeddedDisctionary[r]);
    }

    public Random getRandom() {
        return random;
    }

    public void setRandom(Random random) {
        this.random = random;
    }

}
