package com.xanaduconsultancy.datagenerator;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xanaduconsultancy.datagenerator.model.Config;
import com.xanaduconsultancy.datagenerator.strategies.Strategy;
import com.xanaduconsultancy.sbpc.model.Event;

public class Generator {

    private static final Logger log = LoggerFactory.getLogger(Generator.class);
    private Config config;
    private Map<String, Class<Strategy>> strategies;

    public Generator(Map<String, Class<Strategy>> strategies) {
        this.strategies = strategies;
    }
    
    public List<Event> generateEvents() {
	        
	Random random = new Random(config.getSeed());
        //Map<String, Object> template= config.getTemplate();
        List<Event> events=new LinkedList<>();
        /*Event event=null;
        Strategy strategy=null;
        for(int i=0; i<config.getCount(); i++){
            event=new Event();
            Map<String, Object> idPattern = (Map<String, Object>)template.get("id");
            strategy=selectStrategy(random, idPattern);
            event.setId(strategy.generateData(idPattern, Long.class));
            events.add(event);
        }*/
        return events;
    }

    private Strategy selectStrategy(Random random, Map<String, Object> object) {
        String strategyName=(String) object.get("strategy");
        if(strategyName==null){
            log.error("A strategy must be defined");
        }
        Class<Strategy> strategy=strategies.get(strategyName);
        if(strategy==null){
            log.error("Strategy {} not supported",strategyName);
        }
        try {
            Strategy str=strategy.newInstance();
            str.setRandom(random);
            return str;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Config getConfig() {
        return config;
    }

    public void setConfig(Config config) {
        this.config = config;
    }
}
