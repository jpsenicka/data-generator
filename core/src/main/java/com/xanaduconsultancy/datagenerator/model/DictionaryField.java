//
// Copyright (c) 2011-2015 Xanadu Consultancy Ltd., 
//

package com.xanaduconsultancy.datagenerator.model;


import java.util.List;

public class DictionaryField extends Field {

    private String value;
    private String uri;
    private List<String> embeddedDictionary;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public List<String> getEmbeddedDictionary() {
        return embeddedDictionary;
    }

    public void setEmbeddedDictionary(List<String> embeddedDictionary) {
        this.embeddedDictionary = embeddedDictionary;
    }
}
