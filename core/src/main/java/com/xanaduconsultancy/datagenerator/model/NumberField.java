//
// Copyright (c) 2011-2015 Xanadu Consultancy Ltd., 
//

package com.xanaduconsultancy.datagenerator.model;


public class NumberField extends Field {

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
