package com.xanaduconsultancy.datagenerator.model;

import java.util.List;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class Config {

    @NotNull
    private ConfigType type;

    @NotNull
    @Min(0)
    private int count;

    @NotNull
    @Min(0)
    private long seed;

    @NotNull
    private List<Field> fields;

    public List<Field> getFields() {
        return fields;
    }

    public ConfigType getType() {
        return type;
    }

    public void setType(ConfigType type) {
        this.type = type;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public long getSeed() {
        return seed;
    }

    public void setSeed(long seed) {
        this.seed = seed;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }
}
