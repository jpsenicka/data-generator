//
// Copyright (c) 2011-2015 Xanadu Consultancy Ltd., 
//

package com.xanaduconsultancy.datagenerator.model;


import java.util.Date;

public class DateTimeField extends Field {

    private Date from;
    private Date to;

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }
}
