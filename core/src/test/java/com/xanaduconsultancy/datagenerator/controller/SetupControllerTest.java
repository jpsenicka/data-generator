//
// Copyright (c) 2011-2015 Xanadu Consultancy Ltd., 
//

package com.xanaduconsultancy.datagenerator.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindingResult;
import org.springframework.validation.MapBindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:/application-context.xml", "classpath:/test-context.xml"})
public class SetupControllerTest {

    @Autowired
    private WebApplicationContext webAppCtx;

    @Autowired
    private SetupController setupController;

    private MockMvc mockMvc;

    @Before
    public void before() throws IOException {
        mockMvc = MockMvcBuilders.webAppContextSetup(webAppCtx).build();
    }

    @After
    public void after() {
    }

    @Test
    public void validRequest() throws Exception {
        mockMvc.perform(post("/setup")
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .content(readResource("/config-test1.json")))
            .andExpect(status().isOk());
    }

    @Test
    public void emptyRequest() throws Exception {
        mockMvc.perform(post("/setup")
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .content("[]"))
            .andExpect(status().isOk());
    }

    @Test
    public void notJsonRequest() throws Exception {
        mockMvc.perform(post("/setup")
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .content("#"))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void wrongJsonRequest() throws Exception {
        mockMvc.perform(post("/setup")
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .content("{}"))
            .andExpect(status().isBadRequest());
    }

    @Test
    public void validationErrorHandler() {
        BindingResult result = new MapBindingResult(new HashMap<String, String>(), "");
        result.addError(new ObjectError("ERR", "MSG1"));
        result.rejectValue("F1", "", "MSG2");
        Map<String, String> error = setupController.validationError(new MethodArgumentNotValidException(null, result));
        assertEquals(2, error.size());
        assertEquals("MSG1", error.get("ERR"));
        assertEquals("MSG2", error.get("F1"));
    }

    @Test
    public void badErrorHandler() {
        Map<String, String> error = setupController.generalError(new RuntimeException("ERR"));
        assertEquals(1, error.size());
        assertEquals("ERR", error.get("setupRequest"));
    }

    private String readResource(String name) throws IOException {
        InputStream stream = getClass().getResourceAsStream(name);
        OutputStream out = new ByteArrayOutputStream();
        IOUtils.copy(stream, out);
        return out.toString();
    }


}
