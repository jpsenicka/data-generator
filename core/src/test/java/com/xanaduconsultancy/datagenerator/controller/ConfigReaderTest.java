//
// Copyright (c) 2011-2015 Xanadu Consultancy Ltd., 
//

package com.xanaduconsultancy.datagenerator.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xanaduconsultancy.datagenerator.model.Config;
import com.xanaduconsultancy.datagenerator.model.DateTimeField;
import com.xanaduconsultancy.datagenerator.model.DictionaryField;
import com.xanaduconsultancy.datagenerator.model.NumberField;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ConfigReaderTest {

    private ObjectMapper objectMapper;

    @Before
    public void before() {
        objectMapper = new ObjectMapper();
    }

    @Test
    public void validJson() throws IOException {
        Config[] configs = this.objectMapper.readValue(readResource("/config-test1.json"), Config[].class);
        assertNotNull(configs);
        assertEquals(1, configs.length);
        Config config = configs[0];
        assertEquals(NumberField.class, config.getFields().get(0).getClass());
        assertEquals("$int", ((NumberField)config.getFields().get(0)).getValue());
        assertEquals(DictionaryField.class, config.getFields().get(1).getClass());
        assertEquals("$string vs $string", ((DictionaryField)config.getFields().get(1)).getValue());
        assertEquals(DateTimeField.class, config.getFields().get(2).getClass());
        assertEquals(1262304000000L, ((DateTimeField)config.getFields().get(2)).getFrom().getTime());
        assertEquals(1264982400000L, ((DateTimeField)config.getFields().get(2)).getTo().getTime());
        assertEquals(DictionaryField.class, config.getFields().get(3).getClass());
        assertEquals("$string", ((DictionaryField)config.getFields().get(3)).getValue());
        assertEquals("[OPEN, CLOSED]", ((DictionaryField)config.getFields().get(3)).getEmbeddedDictionary().toString());
    }

    private String readResource(String name) throws IOException {
        InputStream stream = getClass().getResourceAsStream(name);
        OutputStream out = new ByteArrayOutputStream();
        IOUtils.copy(stream, out);
        return out.toString();
    }

}
